# Build Stage
FROM alpine:3.19 AS system

# Install system dependencies
RUN apk update && \
    apk add --no-cache go=1.21.10-r0 && \
    go version && \
    rm -rf /var/cache/apk/*

# Set GOPATH env
ENV GOPATH /app/project/

FROM alpine:3.19 AS build

WORKDIR /app

# Copy the Go module files
COPY project/go.mod .
COPY project/go.sum .

COPY --from=system /usr/bin/go /usr/bin/go
COPY --from=system /usr/lib/go /usr/lib/go
ENV GOROOT /usr/lib/go

# Clear the cache of installed utilities
RUN go clean -modcache && \
    go mod download

# Copy the rest of the application source code
COPY project .

VOLUME /app/view
# Build the Go application
RUN go build -o main ./cmd/main.go

# Expose port 8080 to the outside world
EXPOSE 8080

# Run stage
FROM alpine:3.19 AS segments

COPY --from=build /app/main .

COPY --from=build /app/view /view

COPY --from=build /app/configs /configs

# Command to run the executable
CMD ["./main"]